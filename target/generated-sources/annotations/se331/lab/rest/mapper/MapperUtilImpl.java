package se331.lab.rest.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import se331.lab.rest.dto.ActivityDto;
import se331.lab.rest.dto.AdministratorDto;
import se331.lab.rest.dto.CourseDto;
import se331.lab.rest.dto.LecturerDto;
import se331.lab.rest.dto.StudentDto;
import se331.lab.rest.dto.UserDto;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Administrator;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-12-04T21:38:23+0700",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.1 (Oracle Corporation)"
)
public class MapperUtilImpl implements MapperUtil {

    @Override
    public LecturerDto getLecturerDto(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }

        LecturerDto lecturerDto = new LecturerDto();

        lecturerDto.setId( lecturer.getId() );
        lecturerDto.setName( lecturer.getName() );
        lecturerDto.setSurname( lecturer.getSurname() );
        lecturerDto.setAdvisees( getStudentDto( lecturer.getAdvisees() ) );

        return lecturerDto;
    }

    @Override
    public List<LecturerDto> getLecturerDto(List<Lecturer> lecturer) {
        if ( lecturer == null ) {
            return null;
        }

        List<LecturerDto> list = new ArrayList<LecturerDto>( lecturer.size() );
        for ( Lecturer lecturer1 : lecturer ) {
            list.add( getLecturerDto( lecturer1 ) );
        }

        return list;
    }

    @Override
    public AdministratorDto getAdministratorDto(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }

        AdministratorDto administratorDto = new AdministratorDto();

        administratorDto.setId( administrator.getId() );
        administratorDto.setName( administrator.getName() );
        administratorDto.setSurname( administrator.getSurname() );

        return administratorDto;
    }

    @Override
    public List<AdministratorDto> getAdministratorDto(List<Administrator> administrators) {
        if ( administrators == null ) {
            return null;
        }

        List<AdministratorDto> list = new ArrayList<AdministratorDto>( administrators.size() );
        for ( Administrator administrator : administrators ) {
            list.add( getAdministratorDto( administrator ) );
        }

        return list;
    }

    @Override
    public StudentDto getStudentDto(Student student) {
        if ( student == null ) {
            return null;
        }

        StudentDto studentDto = new StudentDto();

        studentDto.setId( student.getId() );
        studentDto.setStudentId( student.getStudentId() );
        studentDto.setName( student.getName() );
        studentDto.setSurname( student.getSurname() );
        studentDto.setGpa( student.getGpa() );
        studentDto.setImage( student.getImage() );

        return studentDto;
    }

    @Override
    public List<StudentDto> getStudentDto(List<Student> students) {
        if ( students == null ) {
            return null;
        }

        List<StudentDto> list = new ArrayList<StudentDto>( students.size() );
        for ( Student student : students ) {
            list.add( getStudentDto( student ) );
        }

        return list;
    }

    @Override
    public CourseDto getCourseDto(Course course) {
        if ( course == null ) {
            return null;
        }

        CourseDto courseDto = new CourseDto();

        courseDto.setCourseId( course.getCourseId() );
        courseDto.setLecturer( getLecturerDto( course.getLecturer() ) );
        courseDto.setStudents( getStudentDto( course.getStudents() ) );

        return courseDto;
    }

    @Override
    public List<CourseDto> getCourseDto(List<Course> courses) {
        if ( courses == null ) {
            return null;
        }

        List<CourseDto> list = new ArrayList<CourseDto>( courses.size() );
        for ( Course course : courses ) {
            list.add( getCourseDto( course ) );
        }

        return list;
    }

    @Override
    public ActivityDto getActivityDto(Activity activity) {
        if ( activity == null ) {
            return null;
        }

        ActivityDto activityDto = new ActivityDto();

        activityDto.setActivityDate( activity.getActivityDate() );
        activityDto.setActivityDescription( activity.getActivityDescription() );
        activityDto.setActivityLocation( activity.getActivityLocation() );
        activityDto.setActivityName( activity.getActivityName() );
        activityDto.setActivityId( activity.getActivityId() );
        activityDto.setHostTeacher( activity.getHostTeacher() );
        activityDto.setPeriodRegistration( activity.getPeriodRegistration() );
        activityDto.setStudents( getStudentDto( activity.getStudents() ) );

        return activityDto;
    }

    @Override
    public List<ActivityDto> getActivityDto(List<Activity> activities) {
        if ( activities == null ) {
            return null;
        }

        List<ActivityDto> list = new ArrayList<ActivityDto>( activities.size() );
        for ( Activity activity : activities ) {
            list.add( getActivityDto( activity ) );
        }

        return list;
    }

    @Override
    public UserDto getUserDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( user.getId() );
        userDto.setUsername( user.getUsername() );
        userDto.setPassword( user.getPassword() );
        userDto.setFirstname( user.getFirstname() );
        userDto.setLastname( user.getLastname() );
        userDto.setEmail( user.getEmail() );
        userDto.setEnabled( user.getEnabled() );
        userDto.setLastPasswordResetDate( user.getLastPasswordResetDate() );
        userDto.setImage( user.getImage() );
        List<Authority> list = user.getAuthorities();
        if ( list != null ) {
            userDto.setAuthorities( new ArrayList<Authority>( list ) );
        }

        return userDto;
    }

    @Override
    public List<UserDto> getUserDto(List<User> users) {
        if ( users == null ) {
            return null;
        }

        List<UserDto> list = new ArrayList<UserDto>( users.size() );
        for ( User user : users ) {
            list.add( getUserDto( user ) );
        }

        return list;
    }

    @Override
    public CommentDto getCommentDto(Comment comment) {
        if ( comment == null ) {
            return null;
        }

        CommentDto commentDto = new CommentDto();

        commentDto.setId( comment.getId() );
        commentDto.setCommentContent( comment.getCommentContent() );
        commentDto.setCommentor( comment.getCommentor() );
        commentDto.setCommentedDate( comment.getCommentedDate() );
        commentDto.setCommentorImg( comment.getCommentorImg() );

        return commentDto;
    }

    @Override
    public List<CommentDto> getCommentDto(List<Comment> comments) {
        if ( comments == null ) {
            return null;
        }

        List<CommentDto> list = new ArrayList<CommentDto>( comments.size() );
        for ( Comment comment : comments ) {
            list.add( getCommentDto( comment ) );
        }

        return list;
    }

    @Override
    public UserDto getUserDto(Student student) {
        if ( student == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setLastPasswordResetDate( studentUserLastPasswordResetDate( student ) );
        userDto.setImage( studentUserImage( student ) );
        userDto.setFirstname( studentUserFirstname( student ) );
        userDto.setPassword( studentUserPassword( student ) );
        List<Authority> authorities = studentUserAuthorities( student );
        List<Authority> list = authorities;
        if ( list != null ) {
            userDto.setAuthorities( new ArrayList<Authority>( list ) );
        }
        userDto.setEmail( studentUserEmail( student ) );
        userDto.setEnabled( studentUserEnabled( student ) );
        userDto.setUsername( studentUserUsername( student ) );
        userDto.setLastname( studentUserLastname( student ) );
        userDto.setId( student.getId() );

        return userDto;
    }

    @Override
    public UserDto getUserDto(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setLastPasswordResetDate( lecturerUserLastPasswordResetDate( lecturer ) );
        userDto.setImage( lecturerUserImage( lecturer ) );
        userDto.setFirstname( lecturerUserFirstname( lecturer ) );
        userDto.setPassword( lecturerUserPassword( lecturer ) );
        List<Authority> authorities = lecturerUserAuthorities( lecturer );
        List<Authority> list = authorities;
        if ( list != null ) {
            userDto.setAuthorities( new ArrayList<Authority>( list ) );
        }
        userDto.setEmail( lecturerUserEmail( lecturer ) );
        userDto.setEnabled( lecturerUserEnabled( lecturer ) );
        userDto.setUsername( lecturerUserUsername( lecturer ) );
        userDto.setLastname( lecturerUserLastname( lecturer ) );
        userDto.setId( lecturer.getId() );

        return userDto;
    }

    @Override
    public UserDto getUserDto(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setLastPasswordResetDate( administratorUserLastPasswordResetDate( administrator ) );
        userDto.setImage( administratorUserImage( administrator ) );
        userDto.setFirstname( administratorUserFirstname( administrator ) );
        userDto.setPassword( administratorUserPassword( administrator ) );
        List<Authority> authorities = administratorUserAuthorities( administrator );
        List<Authority> list = authorities;
        if ( list != null ) {
            userDto.setAuthorities( new ArrayList<Authority>( list ) );
        }
        userDto.setEmail( administratorUserEmail( administrator ) );
        userDto.setEnabled( administratorUserEnabled( administrator ) );
        userDto.setUsername( administratorUserUsername( administrator ) );
        userDto.setLastname( administratorUserLastname( administrator ) );
        userDto.setId( administrator.getId() );

        return userDto;
    }

    private Date studentUserLastPasswordResetDate(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        Date lastPasswordResetDate = user.getLastPasswordResetDate();
        if ( lastPasswordResetDate == null ) {
            return null;
        }
        return lastPasswordResetDate;
    }

    private String studentUserImage(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String image = user.getImage();
        if ( image == null ) {
            return null;
        }
        return image;
    }

    private String studentUserFirstname(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String firstname = user.getFirstname();
        if ( firstname == null ) {
            return null;
        }
        return firstname;
    }

    private String studentUserPassword(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String password = user.getPassword();
        if ( password == null ) {
            return null;
        }
        return password;
    }

    private List<Authority> studentUserAuthorities(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        List<Authority> authorities = user.getAuthorities();
        if ( authorities == null ) {
            return null;
        }
        return authorities;
    }

    private String studentUserEmail(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String email = user.getEmail();
        if ( email == null ) {
            return null;
        }
        return email;
    }

    private Boolean studentUserEnabled(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        Boolean enabled = user.getEnabled();
        if ( enabled == null ) {
            return null;
        }
        return enabled;
    }

    private String studentUserUsername(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String username = user.getUsername();
        if ( username == null ) {
            return null;
        }
        return username;
    }

    private String studentUserLastname(Student student) {
        if ( student == null ) {
            return null;
        }
        User user = student.getUser();
        if ( user == null ) {
            return null;
        }
        String lastname = user.getLastname();
        if ( lastname == null ) {
            return null;
        }
        return lastname;
    }

    private Date lecturerUserLastPasswordResetDate(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        Date lastPasswordResetDate = user.getLastPasswordResetDate();
        if ( lastPasswordResetDate == null ) {
            return null;
        }
        return lastPasswordResetDate;
    }

    private String lecturerUserImage(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String image = user.getImage();
        if ( image == null ) {
            return null;
        }
        return image;
    }

    private String lecturerUserFirstname(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String firstname = user.getFirstname();
        if ( firstname == null ) {
            return null;
        }
        return firstname;
    }

    private String lecturerUserPassword(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String password = user.getPassword();
        if ( password == null ) {
            return null;
        }
        return password;
    }

    private List<Authority> lecturerUserAuthorities(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        List<Authority> authorities = user.getAuthorities();
        if ( authorities == null ) {
            return null;
        }
        return authorities;
    }

    private String lecturerUserEmail(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String email = user.getEmail();
        if ( email == null ) {
            return null;
        }
        return email;
    }

    private Boolean lecturerUserEnabled(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        Boolean enabled = user.getEnabled();
        if ( enabled == null ) {
            return null;
        }
        return enabled;
    }

    private String lecturerUserUsername(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String username = user.getUsername();
        if ( username == null ) {
            return null;
        }
        return username;
    }

    private String lecturerUserLastname(Lecturer lecturer) {
        if ( lecturer == null ) {
            return null;
        }
        User user = lecturer.getUser();
        if ( user == null ) {
            return null;
        }
        String lastname = user.getLastname();
        if ( lastname == null ) {
            return null;
        }
        return lastname;
    }

    private Date administratorUserLastPasswordResetDate(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        Date lastPasswordResetDate = user.getLastPasswordResetDate();
        if ( lastPasswordResetDate == null ) {
            return null;
        }
        return lastPasswordResetDate;
    }

    private String administratorUserImage(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String image = user.getImage();
        if ( image == null ) {
            return null;
        }
        return image;
    }

    private String administratorUserFirstname(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String firstname = user.getFirstname();
        if ( firstname == null ) {
            return null;
        }
        return firstname;
    }

    private String administratorUserPassword(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String password = user.getPassword();
        if ( password == null ) {
            return null;
        }
        return password;
    }

    private List<Authority> administratorUserAuthorities(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        List<Authority> authorities = user.getAuthorities();
        if ( authorities == null ) {
            return null;
        }
        return authorities;
    }

    private String administratorUserEmail(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String email = user.getEmail();
        if ( email == null ) {
            return null;
        }
        return email;
    }

    private Boolean administratorUserEnabled(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        Boolean enabled = user.getEnabled();
        if ( enabled == null ) {
            return null;
        }
        return enabled;
    }

    private String administratorUserUsername(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String username = user.getUsername();
        if ( username == null ) {
            return null;
        }
        return username;
    }

    private String administratorUserLastname(Administrator administrator) {
        if ( administrator == null ) {
            return null;
        }
        User user = administrator.getUser();
        if ( user == null ) {
            return null;
        }
        String lastname = user.getLastname();
        if ( lastname == null ) {
            return null;
        }
        return lastname;
    }
}
