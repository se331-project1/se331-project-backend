package se331.lab.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.lab.rest.dto.*;
import se331.lab.rest.entity.*;
import se331.lab.rest.security.entity.User;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class );

    @Mappings({})
    LecturerDto getLecturerDto(Lecturer lecturer);
    @Mappings({})
    List<LecturerDto> getLecturerDto(List<Lecturer> lecturer);

    @Mappings({})
    AdministratorDto getAdministratorDto(Administrator administrator);
    @Mappings({})
    List<AdministratorDto> getAdministratorDto(List<Administrator> administrators);

    @Mappings({})
    StudentDto getStudentDto(Student student);
    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    CourseDto getCourseDto(Course course);
    @Mappings({})
    List<CourseDto> getCourseDto(List<Course> courses);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);
    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);
    @Mappings({})
    UserDto getUserDto(User user);
    @Mappings({})
    List<UserDto> getUserDto(List<User> users);

    @Mappings({@Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "email", source = "user.email"),
            @Mapping(target = "username", source = "user.username"),
            @Mapping(target = "firstname",source = "user.firstname"),
            @Mapping(target = "lastname",source = "user.lastname"),
            @Mapping(target = "password",source = "user.password"),
            @Mapping(target = "image",source = "user.image"),
            @Mapping(target = "enabled",source = "user.enabled"),
            @Mapping(target = "lastPasswordResetDate",source = "user.lastPasswordResetDate")})
    UserDto getUserDto(Student student);

    @Mappings({@Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "email", source = "user.email"),
            @Mapping(target = "username", source = "user.username"),
            @Mapping(target = "firstname",source = "user.firstname"),
            @Mapping(target = "lastname",source = "user.lastname"),
            @Mapping(target = "password",source = "user.password"),
            @Mapping(target = "image",source = "user.image"),
            @Mapping(target = "enabled",source = "user.enabled"),
            @Mapping(target = "lastPasswordResetDate",source = "user.lastPasswordResetDate")})
    UserDto getUserDto(Lecturer lecturer);

    @Mappings({@Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "email", source = "user.email"),
            @Mapping(target = "username", source = "user.username"),
            @Mapping(target = "firstname",source = "user.firstname"),
            @Mapping(target = "lastname",source = "user.lastname"),
            @Mapping(target = "password",source = "user.password"),
            @Mapping(target = "image",source = "user.image"),
            @Mapping(target = "enabled",source = "user.enabled"),
            @Mapping(target = "lastPasswordResetDate",source = "user.lastPasswordResetDate")})
    UserDto getUserDto(Administrator administrator);

}
