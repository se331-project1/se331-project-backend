package se331.lab.rest.dao;

import se331.lab.rest.security.entity.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUser();
    User findById(Long id);
    User findByUsername(String username);
    User saveUser(User targetU);

}
