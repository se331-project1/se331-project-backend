package se331.lab.rest.dao;

import se331.lab.rest.entity.Administrator;

import java.util.List;

public interface AdministratorAnotherDao {
    List<Administrator> findAll();
}
