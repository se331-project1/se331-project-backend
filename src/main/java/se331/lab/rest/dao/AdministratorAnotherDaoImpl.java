package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Administrator;
import se331.lab.rest.repository.AdministratorRepository;

import java.util.List;

@Repository
public class AdministratorAnotherDaoImpl implements AdministratorAnotherDao {
    @Autowired
    AdministratorRepository administratorRepository;
    @Override
    public List<Administrator> findAll() {
        return administratorRepository.findAll();
    }
}
