package se331.lab.rest.service;

import se331.lab.rest.security.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUser();
    User findById(Long id);
    User findByUsername(String username);
    User saveUser(User targetU);
}
