package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.AdministratorAnotherDao;
import se331.lab.rest.dto.AdministratorDto;
import se331.lab.rest.entity.Administrator;

import java.util.List;
@Service
public class AdministratorAnotherServiceImpl implements AdministratorAnotherService {
    @Autowired
    AdministratorAnotherDao administratorAnotherDao;
    @Override
    public List<Administrator> getAllAdministrators() {
        return administratorAnotherDao.findAll();
    }
}
