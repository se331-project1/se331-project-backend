package se331.lab.rest.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.UserDao;
import se331.lab.rest.security.entity.User;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService{
    @Autowired
    UserDao userDao;

    @Override
    public List<User> getAllUser() {
        List<User> users = userDao.getAllUser();
        return users;
    }

    @Override
    public User findById(Long id) {
        return userDao.findById(id);
    }


    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User saveUser(User targetU) {
        return userDao.saveUser(targetU);
    }
}
