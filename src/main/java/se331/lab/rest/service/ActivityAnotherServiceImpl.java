package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.ActivityAnotherDao;
import se331.lab.rest.entity.Activity;

import java.util.List;
@Service
public class ActivityAnotherServiceImpl implements ActivityAnotherService {
    @Autowired
    ActivityAnotherDao activityAnotherDao;
    @Override
    public List<Activity> getAllActivities() {
        return activityAnotherDao.getAllActivities();
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityAnotherDao.saveActivity(activity);
    }
}
