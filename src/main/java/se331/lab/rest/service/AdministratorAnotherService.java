package se331.lab.rest.service;

import se331.lab.rest.entity.Administrator;

import java.util.List;

public interface AdministratorAnotherService {
    List<Administrator> getAllAdministrators();
}
