package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.User;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    Long id;
    String username;
    String password;
    String firstname;
    String lastname;
    String email;
    Boolean enabled;
    Date lastPasswordResetDate;
    String image;
    List<Authority> authorities;
}
