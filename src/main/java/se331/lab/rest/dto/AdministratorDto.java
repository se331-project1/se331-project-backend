package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdministratorDto {
    Long id;
    String name;
    String surname;
    List<StudentDto> studentDtos;
    List<ActivityDto> activityDtos;
}
