package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {
    String activityDate;
    String activityDescription;
    String activityLocation;
    String activityName;
    String activityId;
    String hostTeacher;
    String periodRegistration;
    LecturerDto lecturerDto;
    AdministratorDto administratorDto;

    @Builder.Default
    List<StudentDto> students = new ArrayList<>();
}


