package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.Authority;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentRegisterDto {
    Long id;
    String studentId;
    String firstname;
    String lastname;
    Double gpa;
    String image;
    String username;
    String password;
    String email;
    Boolean enabled;
    Date dob;
    Date lastPasswordResetDate;
    List<Authority> authorities;
}
