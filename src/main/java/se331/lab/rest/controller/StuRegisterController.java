package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.dto.StudentRegisterDto;
import se331.lab.rest.entity.Student;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.service.StudentService;
import se331.lab.rest.service.UserService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class StuRegisterController {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthorityRepository authorityRepository;
    @Autowired
    StudentService studentService;

    PasswordEncoder encoder = new BCryptPasswordEncoder();

    @CrossOrigin
    @PostMapping("/sRegister")
    public ResponseEntity<?> saveUser(@RequestBody StudentRegisterDto user) {
        Authority auth3 = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);;
        List<Authority> authorities = new ArrayList<>();
        authorities.add(auth3);
        User userT = User.builder()
                .username(user.getUsername())
                .password(encoder.encode(user.getPassword()))
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .email(user.getEmail())
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .image(user.getImage())
                .build();
        userT = userService.saveUser(userT);
            Student studentT = Student.builder()
                    .studentId(user.getStudentId())
                    .name(user.getFirstname())
                    .surname(user.getLastname())
                    .gpa(4.00)
                    .image(user.getImage())
                    .penAmount(8)
                    .description("Haha")
                    .build();
        userT.setAppUser(studentT);
        studentT =this.studentService.saveStudent(studentT);
        userT = userService.saveUser(userT);

        return ResponseEntity.ok(userT);
}

    // }
}
