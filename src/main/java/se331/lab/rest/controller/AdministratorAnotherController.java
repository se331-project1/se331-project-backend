package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.AdministratorAnotherService;

@Controller
public class AdministratorAnotherController {
    @Autowired
    AdministratorAnotherService administratorAnotherService;
    @GetMapping("/administrator")
    public ResponseEntity getAllAdministrators() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getAdministratorDto(administratorAnotherService.getAllAdministrators()));
    }
}
