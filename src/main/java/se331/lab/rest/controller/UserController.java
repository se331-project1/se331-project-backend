package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import se331.lab.rest.dto.UserDto;
import se331.lab.rest.entity.Student;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;
import se331.lab.rest.service.UserService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller
@Slf4j
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthorityRepository authorityRepository;

    PasswordEncoder encoder = new BCryptPasswordEncoder();

    @CrossOrigin
    @GetMapping("/users")
    public ResponseEntity getAllUser() {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getUserDto(userService.getAllUser()));
    }

    @CrossOrigin
    @GetMapping("/users/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @CrossOrigin
    @PutMapping("/users/{username}")
    public ResponseEntity updateUser(@PathVariable(value = "username") String username, @Valid @RequestBody User userDto){
        User targetU = userService.findByUsername(username);
        targetU.setUsername(username);
        targetU.setPassword(encoder.encode(userDto.getPassword()));
        targetU.setFirstname(userDto.getFirstname());
        targetU.setLastname(userDto.getLastname());
        targetU.setEmail(userDto.getEmail());
        targetU.setEnabled(userDto.getEnabled());
        targetU.setLastPasswordResetDate(Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        targetU.setImage(userDto.getImage());
        targetU.setAuthorities(userDto.getAuthorities());
        targetU.setAppUser(userDto.getAppUser());
        return ResponseEntity.ok(userService.saveUser(targetU));
    }
}
