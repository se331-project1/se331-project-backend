package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Course;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.ActivityAnotherService;

@Controller
public class ActivityAnotherController {
    @Autowired
    ActivityAnotherService activityAnotherService;
    @GetMapping("/activities")
    public ResponseEntity getAllActivities() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityAnotherService.getAllActivities()));
    }
    @PostMapping("/activities")
    public ResponseEntity saveActivity(@RequestBody Activity activity) {
        return ResponseEntity.ok(activityAnotherService.saveActivity(activity));
    }
}
