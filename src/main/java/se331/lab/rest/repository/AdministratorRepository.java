package se331.lab.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Administrator;

import java.util.List;

public interface AdministratorRepository extends CrudRepository<Administrator,Long> {
    List<Administrator> findAll();
}
