package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String activityDate;
    String activityDescription;
    String activityLocation;
    String activityName;
    String activityId;
    String hostTeacher;
    String periodRegistration;

    @ManyToOne
    @JsonBackReference
    Lecturer lecturer;
    @ManyToOne
    @JsonBackReference
    Administrator administrator;
    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    List<Student> students = new ArrayList<>();
}
