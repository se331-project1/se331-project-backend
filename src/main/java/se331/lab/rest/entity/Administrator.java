package se331.lab.rest.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Administrator extends Person {
    @OneToMany(mappedBy = "administrator")
    @Builder.Default
    @ToString.Exclude
    List<Student> students = new ArrayList<>();
    @OneToMany(mappedBy = "administrator")
    @Builder.Default
    @ToString.Exclude
    List<Activity> activities = new ArrayList<>();
}
